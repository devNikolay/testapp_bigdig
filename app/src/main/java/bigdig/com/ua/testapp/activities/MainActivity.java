package bigdig.com.ua.testapp.activities;

import android.content.Context;
import android.os.Bundle;

import bigdig.com.ua.testapp.R;
import bigdig.com.ua.testapp.fragments.BaseFragment;
import bigdig.com.ua.testapp.fragments.VideoListFragment;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment);
    }

    @Override
    protected BaseFragment getInitFragment() {
        return new VideoListFragment();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
