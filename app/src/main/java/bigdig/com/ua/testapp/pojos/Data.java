package bigdig.com.ua.testapp.pojos;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import lombok.experimental.Accessors;

@lombok.Data
@Accessors(prefix = "m")

public class Data  {
    @SerializedName("items")
    private List<Video> mVideo;
}
