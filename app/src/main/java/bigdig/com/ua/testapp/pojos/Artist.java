package bigdig.com.ua.testapp.pojos;

import com.google.gson.annotations.SerializedName;

import lombok.experimental.Accessors;

@lombok.Data
@Accessors(prefix = "m")
public class Artist {
    @SerializedName("name")
    private String mName;
}
