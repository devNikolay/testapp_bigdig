package bigdig.com.ua.testapp.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import bigdig.com.ua.testapp.R;
import bigdig.com.ua.testapp.fragments.BaseFragment;


public abstract class BaseActivity extends AppCompatActivity {
    private FragmentManager mFm = getSupportFragmentManager();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        changeFragment(getInitFragment(), false);
    }

    protected abstract BaseFragment getInitFragment();

    @Override
    public void onBackPressed() {
        mFm.popBackStack();
        if (mFm.getBackStackEntryCount() == 0) {
            super.onBackPressed();
        }
    }

    public void goToTop() {
        mFm.popBackStack(mFm.getBackStackEntryAt(0).getId(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    public void changeFragment(Fragment f, boolean addToBackStack) {
        FragmentTransaction ft = mFm.beginTransaction();

        // Backstack
        if (addToBackStack) {
            ft.addToBackStack(null);
        }

        // Adding fragment
        Fragment oldFragment = mFm.findFragmentById(R.id.fragmentContainer);
        if (oldFragment != null) {
            ft.remove(oldFragment);
        }
        ft.add(R.id.fragmentContainer, f);

        // Commit transaction
        ft.commit();
    }
}
