package bigdig.com.ua.testapp.fragments;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;

import bigdig.com.ua.testapp.activities.BaseActivity;


public abstract class BaseFragment extends Fragment {
    protected BaseActivity mActivity;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Activity) {
            mActivity = (BaseActivity) context;
        }
    }
}
