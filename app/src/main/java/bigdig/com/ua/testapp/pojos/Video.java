package bigdig.com.ua.testapp.pojos;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(prefix = "m")
public class Video {
    @SerializedName("id")
    private int mId;
    @SerializedName("picture")
    private String mPicture;
    @SerializedName("slug")
    private String mSlug;
    @SerializedName("title")
    private String mTitle;
    @SerializedName("view_count")
    private int mViews;
    @SerializedName("artists")
    private ArrayList<Artist> mArtist;
}
