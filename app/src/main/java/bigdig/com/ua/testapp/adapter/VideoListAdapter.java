package bigdig.com.ua.testapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import bigdig.com.ua.testapp.R;
import bigdig.com.ua.testapp.pojos.Video;
import butterknife.Bind;
import butterknife.ButterKnife;

public class VideoListAdapter extends BaseAdapter {

    private List<Video> mVideos = new ArrayList<>();
    private Context mContext;

    public VideoListAdapter (Context context) {
        this.mContext = context;
    }

    @Override
    public int getCount() {
        return mVideos.size();
    }

    @Override
    public Video getItem(int position) {
        return mVideos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder h;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_view_video, parent, false);
            h = new ViewHolder(convertView);
            convertView.setTag(h);
        } else {
            h = (ViewHolder) convertView.getTag();
        }
        h.bind(getItem(position), mContext);

        return convertView;
    }

    public void setVideos(List<Video> videos) {
        mVideos = videos;
        notifyDataSetChanged();
    }

    public static class ViewHolder {
        @Bind(R.id.tv_name_video)
        TextView tvNameVideo;
        @Bind(R.id.tv_artist_name)
        TextView tvArtistName;
        @Bind(R.id.tv_number_views)
        TextView tvNumberViews;
        @Bind(R.id.iv_video)
        ImageView ivVideo;

        public ViewHolder(View v) {
            ButterKnife.bind(this, v);
        }

        public void bind(Video video, Context context ) {
            tvNameVideo.setText(video.getTitle());
            if(!video.getArtist().isEmpty()){
                tvArtistName.setText(video.getArtist().get(0).getName());
            } else {
                tvArtistName.setText("");
            }
            tvNumberViews.setText(video.getViews() + "");
            Picasso.with(context)
                    .load(video.getPicture())
                    .into(ivVideo);
        }

    }
}
