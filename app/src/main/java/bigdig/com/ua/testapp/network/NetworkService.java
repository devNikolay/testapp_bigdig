package bigdig.com.ua.testapp.network;

import bigdig.com.ua.testapp.pojos.Status;
import retrofit.http.GET;
import rx.Observable;

public interface NetworkService {
    @GET("/api/home/video")
    Observable<Status> getStatus();
}
