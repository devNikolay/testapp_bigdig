package bigdig.com.ua.testapp.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import bigdig.com.ua.testapp.R;
import bigdig.com.ua.testapp.adapter.VideoListAdapter;
import bigdig.com.ua.testapp.network.NetworkManager;
import bigdig.com.ua.testapp.network.NetworkService;
import bigdig.com.ua.testapp.pojos.Status;
import butterknife.Bind;
import butterknife.ButterKnife;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class VideoListFragment extends BaseFragment {

    @Bind(R.id.lvVideos)
    ListView lvVideos;


    private VideoListAdapter mAdapter;
    private ProgressDialog dialog;
    private Observable<Status> mObserver;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        dialog = new ProgressDialog(getActivity());
        dialog.show();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_video_list, container, false);
        ButterKnife.bind(this, v);
        mAdapter = new VideoListAdapter(getActivity());
        loadVideos();

        return v;
    }

    private void loadVideos() {
        NetworkService service = NetworkManager.getVideoService();
        mObserver = service.getStatus();
        mObserver.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::setAdapter);

    }

    void setAdapter(Status status) {
        mAdapter.setVideos(status.getData().getVideo());
        lvVideos.setAdapter(mAdapter);
        dialog.dismiss();
    }


    @Override
    public void onDestroy() {
        if (mObserver != null) {
            mObserver.unsubscribeOn(Schedulers.io());
        }
        super.onDestroy();
    }
}
